This is my version of bashpodder.
The original can be found on http://lincgeek.org/bashpodder/

I'm using it in combination with a Sansa Clip+ mp3 player
The player is extended with the Rockbox firmware
http://www.rockbox.org

In addition a 16 gig microsd card contains puppylinux
_AND_ this bashpodder script.

After losing the modifications I made to this script
several times, (Yes I know...) I decided to publish it online on
bitbucket.

Usage:

bp -d <dir> -k <number> -m <number> -h

-d  Directory where you want to keep the podcasts
-k  How many days to keep the podcasts before they are removed
-m  Maximum number of podcasts to fetch from one feed
    (handy for first time use of a feed, or after a period of 
     not downloading the podcasts of a feed)
