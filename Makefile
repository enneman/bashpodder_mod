PROGRAM=bp
PREFIX=$(HOME}/bin

gitpush:
	git push bitbucket master


install: $(PROGRAM)
	@echo "Installing bashpodder..."
	install -s -m 0755 $(PROGRAM) $(PREFIX)/bin
	install -m 0644 $(PROGRAM).conf $(PREFIX)/bin

clean: 
	rm *~
